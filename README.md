# cloudrun-cd

# Overall Idea

The overall idea of this project was to schedule a task of triggering a procedure from Snowflake database that triggers the file in bucket inorder to trigger python scripts in cloud run. Although I have not added the part of Snowflake how the procedures get triggered and placed the file in bucket but the process of automating process in cloud has been added.
Even if anyone is interested on triggering the file into bucket I have added the reference below. And also I have added where I look into to make this project implemented. 

# Use Case Scenario Description

Automating the process of scheduling the task of python scripts based on nodejs module. Also the whole process gets triggers with respect to file uploaded to bucket. 

# Spinning up the cloud services and Steps to follow

## Steps:

### Installation of node package manager
npm install express
npm install body-parser
npm install child_process
npm install @google-cloud/storage

### Cloud Build 
gcloud builds submit \
  --tag gcr.io/$GOOGLE_CLOUD_PROJECT/{image-name}
 
Note:
While building first enable Google Cloud Build API and give view access to its service account

### Deploy Cloud Build
gcloud run deploy {cloudrun-name} \
  --image gcr.io/$GOOGLE_CLOUD_PROJECT/{image-name} \
  --platform managed \
  --region us-central1 \
  --no-allow-unauthenticated
  
### Create Environment Variable for Service URL
SERVICE_URL=$(gcloud run services describe {cloudrun-name} --platform managed --region us-central1 --format="value(status.url)")

### check for non authorization invoking and authorized invoking
curl -X POST $SERVICE_URL
curl -X POST -H "Authorization: Bearer $(gcloud auth print-identity-token)" $SERVICE_URL

### create bucket for the files uploading
gsutil mb gs://$GOOGLE_CLOUD_PROJECT-{bucket-name} \
 --region us-central1

### run this following command inorder to tell Cloud Storage to send a Pub/Sub notification whenever a new file has finished uploading to the docs bucket
gsutil notification create -t new-doc -f json -e OBJECT_FINALIZE gs://$GOOGLE_CLOUD_PROJECT-{bucket-name}

### create a new service account which Pub/Sub will use to trigger the Cloud Run services
gcloud iam service-accounts create pubsub-cloud-run-invoker --display-name "PubSub Cloud Run Invoker"

### Give the new service account permission to invoke the cloud run service
gcloud run services add-iam-policy-binding {cloudrun-name} --member=serviceAccount:pubsub-cloud-run-invoker@$GOOGLE_CLOUD_PROJECT.iam.gserviceaccount.com --role=roles/run.invoker --platform managed --region us-central1

### Find your project number by running this command:
### Look for the project whose name starts with "qwiklabs-gcp-". You will be using the value of the Project Number in the next command.
gcloud projects list

### Create a PROJECT_NUMBER environment variable, replacing [project number] with the Project Number from the last command:
PROJECT_NUMBER=[project number]

### enable your project to create Cloud Pub/Sub authentication tokens:
gcloud projects add-iam-policy-binding $GOOGLE_CLOUD_PROJECT --member=serviceAccount:service-$PROJECT_NUMBER@gcp-sa-pubsub.iam.gserviceaccount.com --role=roles/iam.serviceAccountTokenCreator

### create a Pub/Sub subscription so that the cloud service can run whenever a message is published on the topic "new-doc".
gcloud pubsub subscriptions create {{cloudrun-name}-sub} --topic new-doc --push-endpoint=$SERVICE_URL --push-auth-service-account=pubsub-cloud-run-invoker@$GOOGLE_CLOUD_PROJECT.iam.gserviceaccount.com

# Reference:


- https://docs.snowflake.com/en/user-guide/data-load-snowpipe-auto-gcs.html
- https://medium.com/google-cloud/deploy-to-cloud-run-using-gitlab-ci-e056685b8eeb
- https://www.qwiklabs.com/focuses/8390?parent=catalog
- https://bigdatadave.com/2020/03/21/snowflake-dynamic-unload-path-copy-into-location/


