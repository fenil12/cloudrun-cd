const express    = require('express');
const app        = express();
const bodyParser = require('body-parser');
const {spawn} = require('child_process');

app.use(bodyParser.json());

const port = process.env.PORT || 8080;

app.listen(port, () => {
  console.log('Listening on port', port);
});

app.post('/', async (req, res) => {
  console.log('message incoming:test3');
  let dataToSend;
  try {
    const file = decodeBase64Json(req.body.message.data);
    console.log(`file: ${JSON.stringify(file)}`);
    if(file)
    {
        // spawn new child process to call the python script
        const python = spawn('python', ['script1.py']);
        // collect data from script
        python.stdout.on('data', function (data) {
         console.log('Pipe data from python script ...');
         dataToSend = data.toString();
         console.log('result: ' + dataToSend);
        });
        // in close event we are sure that stream from child process is closed
        python.on('close', (code) => {
        console.log(`child process close all stdio with code ${code}`);
        // send data to browser
        });
    }
  }
  catch (ex) {
    console.log(ex);
  }
  res.set('Content-Type', 'text/plain');
  res.send('\n\nOK\n\n');
})

function decodeBase64Json(data) {
  return JSON.parse(Buffer.from(data, 'base64').toString());
}

